﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	public List<Item> list;
	public GameObject inventory;
	public GameObject inventoryGUI;
	public GameObject container;
	public WearEquip wearequip;
    public Hero player;

	// Use this for initialization
	void Start () {
		list = new List<Item> ();
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		wearequip = GetComponent<WearEquip> ();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Hero>();
	}

	void recalculateItems(){
		int count = list.Count;
		for (int i = 0; i < inventory.transform.childCount; i++) {
			if (inventory.transform.GetChild (i).transform.childCount > 0) {
				Destroy (inventory.transform.GetChild (i).transform.GetChild (0).gameObject);
			}
		}
		for (int i=0;i<count;i++){
			Item it = list [i];
			if (inventory.transform.childCount >= i) {
				GameObject img = Instantiate (container);
				img.transform.SetParent (inventory.transform.GetChild (i).transform);
				img.GetComponent<Image> ().sprite = Resources.Load<Sprite> (it.sprite);
				img.GetComponent<Drag> ().isMeTradeItem = false;
				img.GetComponent<Drag> ().item = it;
			}
			else break;
		}
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				Item item = hit.collider.GetComponent<Item> ();
				if (item != null) {
					list.Add (item);
					Destroy (hit.collider.gameObject);
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.Tab)) {
			if (inventoryGUI.activeSelf) {
				//inventory.SetActive (false);
				inventoryGUI.SetActive (false);

				for (int i = 0; i < inventory.transform.childCount; i++) {
					if (inventory.transform.GetChild (i).transform.childCount > 0) {
						Destroy (inventory.transform.GetChild (i).transform.GetChild (0).gameObject);
					}
				}
			} else {
				//inventory.SetActive (true);
				inventoryGUI.SetActive (true);
				recalculateItems();
			}
		}
	}

	void use(Drag drag){
		if (drag.item.type == "heal") {
			//++health
		}
		else if (drag.item.type == "weapon") {
			GameObject wearedWeapon = GameObject.FindGameObjectWithTag ("weapon");
			if (wearedWeapon != null) {
				Item ww = wearedWeapon.GetComponent<Item> ();
				if (ww != null) {
					list.Add (ww);
					Destroy (wearedWeapon);
				}
			}
			HandItem myitem = Instantiate<GameObject> (Resources.Load<GameObject> (drag.item.prefab)).GetComponent<HandItem> ();
			myitem.tag = "weapon";
			wearequip.addWeapon (myitem);
			//drag.item.transform.SetParent(weaponPlace.transform);
		}

		list.Remove (drag.item);
		//Destroy (drag.gameObject);
		//recalcute();
		recalculateItems ();
	}

	void buy(Drag drag){
        if (player.Gold >= drag.item.BuyPrice) {
            player.Gold -= drag.item.BuyPrice;
            drag.isMeTradeItem = false;
            list.Add(drag.item);
            recalculateItems();
        }
	}

	void sell(Drag drag){
        player.Gold += drag.item.BuyPrice * 0.7f;
		drag.isMeTradeItem = true;
		list.Remove (drag.item);
		recalculateItems ();
	}

	void unwear(Drag drag){
		list.Add (drag.item);
		GameObject wearedWeapon = GameObject.FindGameObjectWithTag ("weapon");
		Destroy (wearedWeapon);
		recalculateItems ();
	}

	public void remove(Drag drag){
		Item it = drag.item;
		GameObject newObject = Instantiate<GameObject>(Resources.Load<GameObject>(it.prefab));
		newObject.transform.position = transform.position + transform.forward - transform.up;
		Destroy (drag.gameObject);
		list.Remove (it);
		Debug.Log ("Removed");
	}
		

}
