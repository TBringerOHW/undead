﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler {

	public Transform canvas;
	public Transform old;
	private GameObject Player;
    private Hero PlayerScript;
	private GameObject Trader;
	public Item item;
	private GameObject weaponPlace;
	private bool isWeared=false;
	public bool isMeTradeItem = false;

	// Use this for initialization
	void Start () {
		canvas = GameObject.Find ("Canvas").transform;
		Player = GameObject.FindGameObjectWithTag ("Player");
        PlayerScript = Player.GetComponent<Hero>();
		Trader = GameObject.FindGameObjectWithTag ("Trader");
		weaponPlace = GameObject.FindGameObjectWithTag ("WeaponPlace");
	}

	public void OnBeginDrag(PointerEventData eventData){
		old = transform.parent;
		transform.SetParent (canvas);
		GetComponent<CanvasGroup> ().blocksRaycasts = false;
	}

	public void OnDrag(PointerEventData eventData){
		transform.position = Input.mousePosition;
	}

	public void OnEndDrag(PointerEventData eventData){
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
		if (transform.parent == canvas) {
			transform.SetParent (old);
		}
	}

	public void OnPointerClick(PointerEventData eventData){
		if (!isMeTradeItem) {
			if (eventData.button == PointerEventData.InputButton.Right) {
				Player.BroadcastMessage ("remove", this);
			} else if (eventData.button == PointerEventData.InputButton.Left && !isWeared) {
				if (eventData.clickCount == 2) {
					if (!Trader.GetComponent<Trader> ().tradeGUI.activeSelf) {
						if (weaponPlace.transform.childCount > 0) {
							weaponPlace.transform.GetChild (0).SetParent (old);
						}
						transform.SetParent (weaponPlace.transform);
						isWeared = true;
						Player.BroadcastMessage ("use", this);
					} else {
						Player.BroadcastMessage ("sell", this);
						Trader.BroadcastMessage ("add", this);
					}
				}
			} else if (eventData.button == PointerEventData.InputButton.Left && isWeared) {
				if (eventData.clickCount == 2) {
					transform.SetParent (old);
					isWeared = false;
					Player.BroadcastMessage ("unwear", this);
				}
			}
		} else {
			if (eventData.clickCount == 2 && PlayerScript.Gold >= item.BuyPrice) {
				isMeTradeItem = false;
				Player.BroadcastMessage ("buy", this);
				Trader.BroadcastMessage ("Remove", this);
			}
		}
	}
}
