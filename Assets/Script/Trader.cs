﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Trader : MonoBehaviour {

	public List<Item> list;
	public GameObject tradeGUI;
	public GameObject panel;
	public GameObject container;
	private FirstPersonController fpcontroller;
	// Use this for initialization
	void Start () {
		tradeGUI.SetActive (false);
		fpcontroller = GameObject.FindGameObjectWithTag ("Player").GetComponent<FirstPersonController> ();
	}

	void recalculate(){
		for (int i = 0; i < panel.transform.childCount; i++) {
			if (panel.transform.GetChild (i).transform.childCount > 0) {
				Destroy (panel.transform.GetChild (i).transform.GetChild (0).gameObject);
			}
		}
		for (int i=0;i<list.Count;i++){
			Item it = list [i];
			if (panel.transform.childCount >= i) {
				GameObject imag = Instantiate (container);
				imag.transform.SetParent (panel.transform.GetChild (i).transform);
				imag.GetComponent<Image> ().sprite = Resources.Load<Sprite> (it.sprite);
				imag.GetComponent<Drag> ().isMeTradeItem = true;
				imag.GetComponent<Drag> ().item = it;
			}
			else break;
		}
	}

	void OnMouseOver(){
		if (Input.GetKeyDown (KeyCode.F) && !tradeGUI.activeSelf) {
			Debug.Log ("LOL");
			tradeGUI.SetActive (true);
			fpcontroller.m_MouseLook.SetSensivity();
			recalculate ();
		}
	}
	// Update is called once per frame
	public void CloseTrade () {
		tradeGUI.SetActive (false);
		fpcontroller.m_MouseLook.SetSensivity();
		for (int i = 0; i < panel.transform.childCount; i++) {
			if (panel.transform.GetChild (i).transform.childCount > 0) {
				Destroy (panel.transform.GetChild (i).transform.GetChild (0).gameObject);
			}
		}
	}

	void Remove(Drag drag){
		list.Remove (drag.item);
		recalculate ();
	}
}
