﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {
	public string type;
	public string sprite;
	public string prefab;
	public float BuyPrice;
	public float SellPrice;
}
