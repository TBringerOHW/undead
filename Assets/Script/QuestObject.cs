﻿using UnityEngine;
using System.Collections;

public class QuestObject : MonoBehaviour {
    private QuestPlayer MP; // опять подключаем скрипт QuestPlayer;

    void Start() {
        MP = GameObject.FindGameObjectWithTag("Player").GetComponent<QuestPlayer>();
    } // определяем что скрипт QuestPlayer будет находится на персонаже с тэгом player;

    void OnMouseUp() {
        Debug.Log("OnMouse");
        //if (Input.GetKeyDown(KeyCode.E)) // если при нажатии на правую кнопку мыши;
          
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            if (MP.ObjectTag == gameObject.tag) // и при том что тэг объекта равен тому значению которое написано в ObjectTag;
           {
                MP.MissionObjects = true; // то переменная принимает значение true, т.е. считается что предмет собран;

                MP.gameObject.GetComponent<Inventory>().list.Add(gameObject.GetComponent<Item>());
                Destroy(gameObject); // и удаляем этот объект со сцены; 
            }
        }
    }
}