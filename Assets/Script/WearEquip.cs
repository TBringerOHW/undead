﻿using UnityEngine;
using System.Collections;

public class WearEquip : MonoBehaviour {
	private HandItem item;
	public Transform hand;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void addWeapon(HandItem it){
		if (item != null) {
			item.transform.SetParent (null);
		}
		it.transform.SetParent (hand);
		it.transform.localPosition = it.position;
		it.transform.localRotation = Quaternion.Euler (it.rotation);
		item = it;
	}
}
