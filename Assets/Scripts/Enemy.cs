﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class Enemy : Entity {

    public int EnemyLvl;
    public float KillExp;

    private GameObject Player;

    void Start() {
        Player = GameObject.FindGameObjectWithTag("Player");
        Defense = ((Armor) * 0.06f) / (1f + (0.06f * (Armor)));
    }
	
	// Update is called once per frame
	void Update () {
        if (HitPoints <= 0 && !isDead) Die();
    }

    private void Die() {
        isDead = true;
        Player.GetComponent<Hero>().Exp += KillExp;
    }    
}
