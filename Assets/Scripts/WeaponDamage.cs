﻿using UnityEngine;
using System.Collections.Generic;

public class WeaponDamage : MonoBehaviour {


    public float attackRange = 0.5f;
    public float Radius = 0.1f;

    private HashSet<GameObject> enemies;
    public Transform direction;

    public void Start() {
        direction = transform.FindChild("direction");
    }

    private Transform Direction {
        get {
            if (direction == null) {
                direction = transform.Find("direction");
            }
            return direction;
        }
    }

    public void Prepare() {
        Debug.ClearDeveloperConsole();
        enemies = new HashSet<GameObject>();
    }

    public void CheckCollides() {
        RaycastHit HitInfo;
        Debug.DrawRay(Direction.position, Direction.up * attackRange, Color.red, 5f, false);
        if (Physics.SphereCast(Direction.position, Radius, Direction.up, out HitInfo, attackRange)) {
            if (gameObject.transform.root.tag != HitInfo.transform.root.tag) {//&& (HitInfo.collider.gameObject.tag == "Enemy" || HitInfo.collider.gameObject.tag == "Player"
                if (!enemies.Contains(HitInfo.collider.gameObject) && HitInfo.collider.gameObject.GetComponent<Entity>() != null) {
                    enemies.Add(HitInfo.collider.gameObject);
                }                
            }
        }
            
    }

    public void Damage() {
        if (enemies.Count >= 1)
            foreach (GameObject enemy in enemies) {
                //&& gameObject != HitInfo.collider.gameObject && gameObject.transform.root.gameObject != HitInfo.collider.gameObject

                float Damage = gameObject.GetComponentInParent<Entity>().Damage;
                float Defense = enemy.GetComponent<Entity>().Defense;
                float ReducedDamage = (Damage * Defense) == 0 ? Damage : (Damage * (1 - Defense));

                enemy.GetComponent<Entity>().TakeDamage(Damage);

                //Debug.Log(enemy.name + " take " + (Damage) + "(" + (ReducedDamage) + ")" + " damage from " + gameObject.name);
            }
            
    }
}
