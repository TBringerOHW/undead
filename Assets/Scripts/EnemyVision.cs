﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class EnemyVision : MonoBehaviour {

    private EnemyAi EnemyAI;

	// Use this for initialization
	void Awake () {
        EnemyAI = transform.GetComponentInParent<Enemy>(). gameObject.GetComponent<EnemyAi>();
    }

    void OnTriggerEnter(Collider col) {
        if (col.gameObject.tag == "Player") EnemyAI.SetTarget(col.transform);
    }
}
