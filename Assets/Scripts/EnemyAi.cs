using System;
using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class EnemyAi : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for

        private Vector3 MovePoint;
        private bool Roaming;
        private bool Attacking;
        private bool PlayerSpoted;
        private float TimeSinceLastRoam;
        private float RoamTimer = 3f;
        private Vector3 SpawnPoint;

        public float Distance;
        private GameObject Player;
        private Enemy thisEntity;


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
            MovePoint = gameObject.transform.position;
            SpawnPoint = MovePoint;

            Roaming = true;

	        agent.updateRotation = false;
	        agent.updatePosition = true;

            Player = GameObject.FindGameObjectWithTag("Player");
            thisEntity = gameObject.GetComponent<Enemy>();
        }


        private void Update() {
            if (!thisEntity.isDead) {
                if (gameObject)
                    TimeSinceLastRoam += Time.deltaTime;
                RoamTimer = UnityEngine.Random.Range(1f, 5f);

                if (target != null) {
                    agent.SetDestination(target.position);
                    Roaming = false;
                    PlayerSpoted = true;
                    character.Move(agent.desiredVelocity, false, PlayerSpoted);
                    if (!target.gameObject.activeInHierarchy) { target = null; PlayerSpoted = false; }
                }
                else
                if (target == null) {
                    if (TimeSinceLastRoam >= RoamTimer) Roam();
                    if (MovePoint == gameObject.transform.position && target == null) Roaming = true;
                    if (Vector3.Distance(gameObject.transform.position, SpawnPoint) > 5f) {
                        agent.SetDestination(MovePoint);
                        character.Move(agent.velocity, false, PlayerSpoted);
                        Distance = Vector3.Distance(gameObject.transform.position, SpawnPoint);
                    }

                }

                //character.Move(Vector3.zero, false, false);
                if (agent.remainingDistance > agent.stoppingDistance)
                    character.Move(agent.desiredVelocity, false, PlayerSpoted);
                else {
                    if (target != null) character.Move(Vector3.zero, true, false);
                    else character.Move(Vector3.zero, false, PlayerSpoted);
                }
            }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        public void Roam() {
            TimeSinceLastRoam = 0f;

            MovePoint = new Vector3(
                gameObject.transform.position.x + (4 - UnityEngine.Random.Range(0f, 8f)),
                gameObject.transform.position.y,
                gameObject.transform.position.z + (4 - UnityEngine.Random.Range(0f, 8f)) );

            agent.SetDestination(MovePoint);
            character.Move(agent.velocity, false, PlayerSpoted);
            Roaming = false;
        }
    }
}
