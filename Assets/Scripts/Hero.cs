﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Hero : Entity {

    private float MaxHitPoints = 2000f;

    public float Exp;
    public float Gold;
    public int Lvl;
    public float ReqExp = 1000;
    public int FreePoints = 0;

    public Quest quest;

    public Slider SliderHP;
    public Text LEVEL;

    public float BaseDamage;

    public int Str;
    public int Agi;
    public int Int;

    // Use this for initialization
    void OnAwake() {
        
        
    }

    void Start() {

        HitPoints = MaxHitPoints;

        BaseDamage = Damage;
        Armor += Agi / 7;
        Damage = BaseDamage + Str;

        Defense = ((Armor) * 0.06f) / (1f + (0.06f * (Armor)));
    }
	
	// Update is called once per frame
	void Update () {
        if (HitPoints <= 0) Die();
        if (Exp >= ReqExp) LevelUp();
        SliderHP.value = Mathf.Lerp(0, 1, HitPoints/MaxHitPoints);
        //Debug.Log(Mathf.Lerp(0, 1, HitPoints / 100));
    }

    void LevelUp() {
        Exp -= ReqExp;
        float IntModifier = Int;
        float CurrentLevel = Lvl;
        ReqExp = ( ( (CurrentLevel + 1f) * 100f + 50f ) * ((100f - IntModifier) / 100f) ) + 1000;
        Lvl++;
        LEVEL.text = Lvl + "";
        Str += 3;
        Damage = BaseDamage + Str;
    }

    private void Die() {
        gameObject.SetActive(false);
    }
}
