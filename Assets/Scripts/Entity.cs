﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour {

    public bool isDead;

    public float HitPoints;
    public float Damage;
    public float Armor;
    public float Defense;

    public void TakeDamage(float Dmg) {
        HitPoints -= (Dmg * Defense) == 0 ? Dmg : (Dmg * (1 - Defense));
        Debug.Log(gameObject.name + " take " + Dmg + " damage");
    }


}
