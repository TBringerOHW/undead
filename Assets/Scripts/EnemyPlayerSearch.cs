﻿using UnityEngine;
using System.Collections;

public class EnemyPlayerSearch : MonoBehaviour {

    public Transform Player;
    
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Update () {
        StartCoroutine("Search");
    }

    IEnumerator Search() {
        while (true) {
            Vector3 EnemyForward = Vector3.Scale(gameObject.transform.forward, new Vector3(1, 0, 1));
            if (Mathf.Abs(Vector3.Angle(EnemyForward, Player.transform.position)) >= 180 || Vector3.Distance(Player.position, gameObject.transform.position) <= 10f) {
                gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.EnemyAi>().target = Player;
            }
            yield return new WaitForSeconds(Vector3.Distance(Player.position, transform.position)/2f);
        }
    }
}
