﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("QuestCollection")]
public class QuestContainer {
    [XmlArray("Quests")]
    [XmlArrayItem("Quest")]
    //public Dictionary<string, Quest> Quests = new Dictionary<string, Quest>();
    public List<Quest> Quests = new List<Quest>();

    public void Save(string path) {
        var serializer = new XmlSerializer(typeof(QuestContainer));
        using (var stream = new FileStream(path, FileMode.Create)) {
            serializer.Serialize(stream, this);
        }
    }

    public static QuestSystem PrepareToWork(QuestContainer container) {
        QuestSystem qs = new QuestSystem();
        foreach (Quest quest in container.Quests) {
            if (!qs.Quests.ContainsKey(quest.npcID))
                qs.Quests.Add(quest.npcID, new Dictionary<int, Quest>());
            qs.Quests[quest.npcID].Add(quest.npcQuestStage, quest);
        }
        return qs;
    }
    //public static QuestContainer PrepareToSave(QuestSystem system) {
    //    QuestContainer qc = new QuestContainer();
    //    foreach (KeyValuePair<int, Dictionary<int, Quest>> dictionary in system.Quests) {
    //        foreach (KeyValuePair<int, Quest> quest in dictionary.Value) {
    //            qc.Quests.Add(quest.Value);
    //        }
    //    }
    //    return qc;
    //}

    public static QuestContainer Load(string path) {
        var serializer = new XmlSerializer(typeof(QuestContainer));
        using (var stream = new FileStream(path, FileMode.Open)) {
            return serializer.Deserialize(stream) as QuestContainer;
        }
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public static QuestContainer LoadFromText(string text) {
        var serializer = new XmlSerializer(typeof(QuestContainer));
        return serializer.Deserialize(new StringReader(text)) as QuestContainer;
    }
}