﻿using UnityEngine;
using System.Collections;
using System.IO;

public class QuestController : MonoBehaviour {

    QuestSystem qs;

	// Use this for initialization
	void Awake () {
        QuestContainer Container = new QuestContainer();
        Container = QuestContainer.Load(Path.Combine(Application.persistentDataPath, "E:/Unity/Projects/PP/Assets/Resources/Quests/quests.xml")); Debug.Log("Quests Loaded");
        foreach (Quest quest in Container.Quests) {
            Debug.Log(quest.name + " " + quest.npcID + " " + quest.npcQuestStage);
        }
        
        qs = QuestContainer.PrepareToWork(Container);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public Quest getQuest(int npcID, int QuestStage) {
        if (qs.Quests.ContainsKey(npcID)) {
            if (qs.Quests[npcID].ContainsKey(QuestStage)) {
                Debug.Log(qs.Quests[npcID][QuestStage].name + " Quest recived by NPC " + npcID + " With quest tag " + qs.Quests[npcID][QuestStage].TargetTag);
                return qs.Quests[npcID][QuestStage];
            }
            else {  return null; } //Debug.Log("There is no such quest.");
        }
        return null;
    }
}
