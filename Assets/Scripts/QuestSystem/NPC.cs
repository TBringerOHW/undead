﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {

    public int npcID;
    public int npcQuestStage = 0;

    private Hero player;
    private QuestController qc;
    private Quest currentQuest = null;
    private bool isNoQuestsLeft;
    

    // Use this for initialization
    void Start () {
        qc = GameObject.FindGameObjectWithTag("QuestController").GetComponent<QuestController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Hero>();
        if (currentQuest == null) { currentQuest = qc.getQuest(npcID, npcQuestStage); }      
    }

    // Update is called once per frame
    void Update () {       
        if (Input.GetKeyDown(KeyCode.F)) {
            //currentQuest = qc.getQuest(0, 0);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                if (hit.collider.gameObject == gameObject)
                    Talk();
            }
        }
    }

    void Talk() {
        if (!isNoQuestsLeft) {
           // Debug.Log(GameObject.FindGameObjectsWithTag(currentQuest.TargetTag).Length + " " + currentQuest.reward + " " + currentQuest.name);
            if (GameObject.FindGameObjectsWithTag(currentQuest.TargetTag).Length == 0) {
                print("There is You reward: " + currentQuest.reward);
                player.Gold += currentQuest.reward;
                player.Exp += currentQuest.reward / 2;
                
                npcQuestStage++;
                if (qc.getQuest(npcID, npcQuestStage) == null) { currentQuest = null; isNoQuestsLeft = true; return; }
                Debug.Log("Taking new quest! QStage = " + npcQuestStage);
                currentQuest = qc.getQuest(npcID,npcQuestStage); 
            }
            else
                print("Quest discription: " + currentQuest.discription);
        }
        else print("No active quest.");
    }

}
