﻿using UnityEngine;
using System.Collections;

public class BurnableTreeBranch : MonoBehaviour {

    public string TreeName = "Oak";
    [Range(0, 900)]
    public float CurrentTemperature = 25f;

    private bool FireRaised = false;

    private Vector2 WindDirection;

    private float MaximumTemperature;
    private float FlameHeat;//Зависит от типа горящей древесины и /текущих погодных условий(?)
    private float EnvironmentDampness;
    private float TreeDampness;
    public float TreeStructure;
    private float CurrentFlameHeat;//
    private float PrevSearchTemp;

    private WoodType TypeOfWood;

    Collider[] BurnedTrees;
    ArrayList BTList = new ArrayList();

    private Conditions ConditionsManager;

    private MeshParticleEmitter Emitter;

    void Start() {
        ConditionsManager = GameObject.FindGameObjectWithTag("Conditions Manager").GetComponent<Conditions>();

        ConditionsManager.Trees++;

        WindDirection = ConditionsManager.WindDirection;
        TypeOfWood = ConditionsManager.getWoodProperties(TreeName);
        MaximumTemperature = TypeOfWood.MaximumTemperature;
        EnvironmentDampness = ConditionsManager.Dampness;

        if (EnvironmentDampness * 0.7f > 0.5f) TreeDampness = Random.Range(0.5f, EnvironmentDampness);
        else TreeDampness = 0.5f;

        FlameHeat = (TypeOfWood.FlameHeat * (1.0f - EnvironmentDampness)) * (1.0f - TreeDampness);

        TreeStructure = TypeOfWood.WoodMediumHeight * gameObject.transform.localScale.magnitude * 0.3f * 10 * TypeOfWood.WoodDensity * 100;

        PrevSearchTemp = CurrentTemperature;
    }

    void Update() {
        if (FireRaised) {
            HeatClosestTree();
            ReduceStructure();
            Burn();
        }
    }

    void FixedUpdate() {
        if (ConditionsManager.ReRand) {
            if (ConditionsManager.RandTreeRotation)
                gameObject.transform.rotation =
                    new Quaternion(Random.Range(5.0f, 10.0f), Random.Range(0.0f, 360.0f), Random.Range(5.0f, 10.0f), Random.Range(0.0f, 2.0f));
            if (ConditionsManager.RandTreeScale) {
                float rnd = Random.Range(0.6f, 1.4f);
                gameObject.transform.localScale = new Vector3(rnd, rnd, rnd);
            }
            ConditionsManager.ReRandedTrees++;
        }
        if (CurrentTemperature > 275f && !FireRaised) { RaiseFire(); FindTrees(); }        
        if (WindDirection != ConditionsManager.WindDirection && FireRaised) {
            WindDirection = ConditionsManager.WindDirection;
            Emitter.worldVelocity = new Vector3(WindDirection.x/2, Emitter.worldVelocity.y/2, WindDirection.y/2);
        }
    }

    private void ReduceStructure() {
        TreeStructure += -(ConditionsManager.Speed + FlameHeat / 10000 + (CurrentTemperature / 2000)) * ConditionsManager.Speed * 2;
        if (TreeStructure <= 0) {
            FindTrees();
            gameObject.SetActive(false);            
        }
        
    }

    private void RaiseFire() {
        Emitter = gameObject.GetComponent<MeshParticleEmitter>();
        Emitter.emit = true;
        Emitter.worldVelocity = new Vector3(WindDirection.x, Emitter.worldVelocity.y, WindDirection.y);
        if (!gameObject.name.Equals("Trunk")) Emitter.maxEmission = 50;
        ConditionsManager.BurningTrees++;
        FireRaised = true;
    }

    private void ChangeTreeDampness() {
        if (TreeDampness > CurrentTemperature / ((MaximumTemperature * 0.75f) * 0.01f))
            TreeDampness = CurrentTemperature / ((MaximumTemperature * 0.75f) * 0.01f);
        FlameHeat = (TypeOfWood.FlameHeat * (1.0f - EnvironmentDampness * 0.25f)) * (1.0f - TreeDampness);
    }

    private void FindTrees() {
        BurnedTrees = Physics.OverlapSphere(gameObject.transform.position + new Vector3(WindDirection.x, 0f, WindDirection.y), 2f + CurrentTemperature / 150f);
        BTList = new ArrayList();
        for (int i = 0; i < BurnedTrees.Length; i++) {
            if (BurnedTrees[i].CompareTag("Branch")) { BTList.Add(BurnedTrees[i].GetComponentInParent<BurnableTreeBranch>()); }
        }
        PrevSearchTemp = CurrentTemperature;        
    }

    private void Burn() {
        if (CurrentTemperature < MaximumTemperature) {
            CurrentTemperature += ConditionsManager.Speed + FlameHeat / 10000 + (CurrentTemperature / 2000);
            ChangeTreeDampness();
        }
    }

    private void HeatClosestTree() {        
        if (CurrentTemperature - PrevSearchTemp > 75f) FindTrees();
        if (BTList.Count != 0)
            for (int i = 0; i < BTList.Count; i++) {                     
                BurnableTreeBranch BurnScript =(BurnableTreeBranch) BTList[i];
                if (BurnScript != null)
                if (BurnScript.CurrentTemperature < BurnScript.MaximumTemperature) {               
                        float TargetDistance = Vector3.Distance(gameObject.transform.position, BurnScript.gameObject.transform.position);
                        if (TargetDistance != 0)
                        BurnScript.ChangeTemperature(ConditionsManager.Speed + FlameHeat / 10000 + (CurrentTemperature / 2000) - TargetDistance / 100);// - TargetDistance / 100 0.4205 to 0.974    -0.2 --> 0.2205 to 0.774 -0.2 --> 0.0205 to 0.574
                        if (BurnScript.FireRaised)
                            BTList.RemoveAt(i);
                    }
            }
    }

         /*    if (BurnedTrees[i].gameObject.CompareTag("Branch"))
                if (BurnedTrees[i].gameObject.GetComponentInParent<BurnableTreeBranch>().CurrentTemperature<BurnedTrees[i].gameObject.GetComponentInParent<BurnableTreeBranch>().MaximumTemperature) {
                    float TargetDistance = Vector3.Distance(gameObject.transform.position, BurnedTrees[i].gameObject.transform.position);
                    if (TargetDistance != 0)
                        BurnedTrees[i].gameObject.GetComponentInParent<BurnableTreeBranch>().ChangeTemperature(ConditionsManager.Speed + FlameHeat / 10000 + (CurrentTemperature / 2000) - TargetDistance / 100);
*/
    public void ChangeTemperature(float value) { CurrentTemperature += value; }
}
