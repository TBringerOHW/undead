﻿using UnityEngine;
using System.Collections;
using System;

public class Conditions : MonoBehaviour {

    public Boolean RandTreeRotation;
    public Boolean RandTreeScale;

    public Boolean ReRand;
    [HideInInspector]
    public Vector2 WindDirection;
    [Range(-10.0f, 10.0f)]
    public float WindDirectionX;
    [Range(-10.0f, 10.0f)]
    public float WindDirectionY;

    [Range(0.0f, 1.0f)]
    public float Dampness;

    [Range(0.0f, 4.0f)]
    public float Speed;

    public int Trees;
    public int BurningTrees;

    public WoodType woodType;

    private Vector2 currentWindDirection;
    [HideInInspector]
    public int ReRandedTrees;

	void Start () {
        currentWindDirection = WindDirection;
        woodType = new WoodType();
	}
	
	void Update () {
        WindDirection.x = WindDirectionX; WindDirection.y = WindDirectionY;
        if (currentWindDirection != WindDirection) currentWindDirection = WindDirection;
        if (ReRandedTrees >= Trees) { ReRand = false; ReRandedTrees = 0; }
	}


    public WoodType getWoodProperties(string WoodName) {
        WoodType type = new WoodType();
        SetPropeties(type, 900, 3240, 0.810f, 50f, "Oak");//oak by default
        switch (WoodName) {
            case "Oak":
                SetPropeties(type, 900, 3240, 0.810f, 50f, "Oak");
                return type;
            case "Brich":
                SetPropeties(type, 816, 3000, 0.750f, 35f, "Brich");
                return type;
            case "Pine":
                SetPropeties(type, 624, 2080, 0.520f, 45f, "Pine");
                return type;
            default:
                Debug.Log("Wrong wood type name! " + WoodName);
                break;
        }        
        return type;
    }

    private void SetPropeties(WoodType type, float maxTemp, float heat, float density,float height, string name) {
        type.MaximumTemperature = maxTemp;
        type.FlameHeat = heat;
        type.WoodDensity = density;
        type.WoodMediumHeight = height;
        type.WoodTypeName = name;
    }


}
