﻿public class WoodType {
    public float MaximumTemperature;//Максимальная температура горения
    public float FlameHeat;//Теплотворность древесины ккал/кг
    public float WoodDensity;//Плотность, (кг/дм3) 
    public float WoodMediumHeight;
    public string WoodTypeName;    
}
//h = 20м, Rосн = 0,3м дерево ~ 60дм3()