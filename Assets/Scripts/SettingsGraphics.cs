﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingsGraphics : MonoBehaviour {

	public Slider sliderQuality, sliderResolution;
	public int resolution, quality;
	public bool fullscreen;
	public Toggle fullscreenToggle;


	// Use this for initialization
	void Start () {
//		
	}
	
	// Update is called once per frame
	 void Update () {
		SettingsQuality();
	}

	void SettingsQuality(){

		if (sliderResolution.maxValue != Screen.resolutions.Length)
			sliderResolution.maxValue = Screen.resolutions.Length;
				
		if (quality != sliderQuality.value)
			quality = (int)sliderQuality.value;

		if (resolution != sliderResolution.value)
			resolution = (int)sliderResolution.value;

		if (fullscreen != fullscreenToggle.isOn)
			fullscreen = fullscreenToggle.isOn;

		sliderQuality.transform.FindChild ("Quality").GetComponent<Text> ().text = "Quality: " + quality;
		sliderResolution.transform.FindChild ("Resolution").GetComponent<Text>().text = "Resolution: " + Screen.resolutions[resolution].width + " x " + Screen.resolutions[resolution].height;

	}


		public void Accept() {
			Screen.SetResolution (Screen.resolutions[resolution].width, Screen.resolutions[resolution].height,fullscreen);
			QualitySettings.SetQualityLevel(quality);
	}
	public void Quit() {
		Application.Quit();
	}
}
