﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioListener))]
[RequireComponent(typeof(AudioSource))]

public class SettingsMusic : MonoBehaviour {
	public Slider sliderMusic;
	public float volumeMusic;
	public AudioClip music;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		SettingMusic ();
	 
	}

	void SettingMusic() {
		if (gameObject.GetComponent<AudioSource>().clip == null) {
			gameObject.GetComponent<AudioSource>().clip = music;
		}

		if(volumeMusic != (float)sliderMusic.value/100) {
			volumeMusic = (float)sliderMusic.value/100;
		}


		if(!gameObject.GetComponent<AudioSource>().isPlaying) {
			gameObject.GetComponent<AudioSource> ().PlayOneShot(music);
	}
		gameObject.GetComponent<AudioSource> ().loop = true;

	}

	public void Accept() {
		gameObject.GetComponent<AudioSource>().volume = volumeMusic; 
	}
}
