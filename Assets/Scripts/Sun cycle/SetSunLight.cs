﻿using UnityEngine;
using System.Collections;

public class SetSunLight : MonoBehaviour {

    public Material sky;
    private Transform Stars;

    // Use this for initialization
    void Start() {
        Stars = GameObject.FindGameObjectWithTag("Stars").transform;
        sky = RenderSettings.skybox;
    }
    

    // Update is called once per frame
    void Update() {
        Stars.transform.rotation = transform.rotation;
    }
}