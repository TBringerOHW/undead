﻿using UnityEngine;
using System.Collections;

public class ScriptForMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Quit() {
		Application.Quit();
	}

	public void Resume(){
		Application.LoadLevel("Game");
	}

	public void InMenu(){
		Application.LoadLevel("Menu");
	}
}
