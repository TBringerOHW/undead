﻿using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour {

	Renderer r;
	MovieTexture movie;

	// Use this for initialization
	void Start () {
		r = GetComponent<Renderer>();
		movie = (MovieTexture)r.material.mainTexture;
	
	
	}
	
	// Update is called once per frame
	void Update () {
		


		if (!movie.isPlaying) {
			movie.Play();
			}
			

		movie.loop = true;		

	}
}

