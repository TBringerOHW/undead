﻿using UnityEngine;
using System.Collections;

public class FireController : MonoBehaviour {

    public float speed = 0.2f;
    public float maxHeight;

    private Vector3 InitPos;
    private GameObject ParentTree;
    private float TreeTemperature = 25f; //25 - 900
    private BurnableTree TreeScript;

	void Start () {
        InitPos = gameObject.transform.position;
        TreeScript = GetComponentInParent<BurnableTree>();
    }
	
	void Update () {
        TreeTemperature = TreeScript.CurrentTemperature;
        gameObject.transform.position += new Vector3(0.0f, speed, 0.0f);
        if (gameObject.transform.position.y > TreeTemperature/50) gameObject.transform.position = InitPos;
    }
}
