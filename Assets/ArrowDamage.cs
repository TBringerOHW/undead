﻿using UnityEngine;
using System.Collections;

public class ArrowDamage : MonoBehaviour {

    public Hero player;

    private Rigidbody rg;



	// Use this for initialization
	void Start () {
        //rg = gameObject.GetComponent<Rigidbody>();
	}

    void Awake() {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Hero>();
        gameObject.GetComponent<Rigidbody>().AddForce(GameObject.FindGameObjectWithTag("BowDirection").transform.up * 1000f);
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnCollisionEnter(Collision other) {
        if (other.gameObject.GetComponent<Enemy>() != null) {
            other.gameObject.GetComponent<Enemy>().TakeDamage(player.Damage);            
        }
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        SelfDestroy(5f);
    }

    IEnumerator SelfDestroy(float time) {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
     }


}
